# coding=utf-8
from time import sleep
import random
import logging


from silf.backend.michelson.api import MichelsonAPI


class MockMichelsonDriver(MichelsonAPI):
    def __init__(self, deviceFilePath, readTimeout):
        super(MockMichelsonDriver, self).__init__()
        self.pos = 0
        self.voltage = 0
        self.logger = logging.getLogger('michelson')
        self.slope = 0

    def calibration(self):
        self.pos = 0
        self.slope = random.randint(10, 90) / 100
        pass

    def set_pos(self, pos):
        self.logger.debug("Set vel: %s", pos)
        self.pos = pos

    def get_voltage(self):
        self.voltage = self.pos * self.slope
        self.logger.debug("Get voltage: %s", self.voltage)
        return self.voltage

    def get_pos(self):
        self.logger.debug("Get pos: %s", self.pos)
        return self.pos

    def stop(self):
        pass

    def is_open(self):
        return True

    def close(self):
        pass
