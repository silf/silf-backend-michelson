# coding=utf-8

from datetime import timedelta
from silf.backend.commons.api.stanza_content import NumberControl, OutputFieldSuite, OutputField, ChartField, ControlSuite
from silf.backend.commons.device_manager._result_creator import XYChartResultCreator

from silf.backend.commons.simple_server.simple_server import MultiModeManager, ExperimentMode, ResultManager

from silf.backend.michelson.michelson_driver import MichelsonDriver
from silf.backend.michelson.mock_michelson_driver import MockMichelsonDriver

import configparser
import os
import typing
from enum import Enum
import logging

from silf.backend.commons.api.translations.trans import _


class NormalMode(ExperimentMode):
    name = 'michelson'
    label = 'Michelson'
    device_id = 'Michelson'

    result = {'position': 0, 'voltage': 0}

    class State(Enum):
        calibrate = 1
        set_position = 2
        measure = 3

    def __init__(self, config_parser, experiment_callback):
        super().__init__(config_parser, experiment_callback)
        self.started = False
        self.state = None
        self.michelson_driver = None
        self.logger = logging.getLogger('mode')

        self.point = 0
        self.result_manager = ResultManager(self.create_result_creators(), self.experiment_callback)

    def start(self):
        self.state = None
        self.point = 0
        self.result_manager.clear()
        self.started = True

    def stop(self):
        self.started = False
        self.result_manager.clear()

    def tear_down(self):
        self.power_down()

    def power_down(self):
        self.michelson_driver = None

    def power_up(self):
        self.michelson_driver = self.DriverToUse(self.config[self.device_id]['filePath'],
                                                 int(self.config[self.device_id]['timeout']))

    def _state_transition(self, state):
        self.logger.debug("state {} -> {}".format(self.state, state))
        self.state = state

    def _calibrate(self):
        self.michelson_driver.calibration()
        self.point = 0
        self._state_transition(self.State.set_position)

    def _set_position(self):
        position = self.settings['start_pos'] + self.point * self.settings['step']

        # exit point
        if position > self.settings['end_pos']:
            self.experiment_callback.send_series_done()
            self.started = False
            self._state_transition(None)
        else:
            self.michelson_driver.set_pos(position)
            self._state_transition(self.State.measure)

    def _measure(self):
        self.result = {'position': self.michelson_driver.get_pos(),
                       'voltage': self.michelson_driver.get_voltage()}
        self.point += 1
        self.result_manager.push_results(self.result)

        self._state_transition(self.State.set_position)

    @property
    def DriverToUse(self):
        if self.config.getboolean(self.device_id, "use_mock",
                                  fallback=bool(os.environ.get('MICHELSON_USE_MOCK', False))):
            return MockMichelsonDriver
        else:
            return MichelsonDriver

    @classmethod
    def get_description(cls) -> str:
        pass

    @classmethod
    def get_mode_label(cls) -> str:
        return cls.label

    @classmethod
    def get_mode_name(cls) -> str:
        return cls.name

    def loop_iteration(self):
        if self.started:
            if self.state == self.State.calibrate:
                self._calibrate()
            elif self.state == self.State.set_position:
                self._set_position()
            elif self.state == self.State.measure:
                self._measure()
            else:
                print("--- loop iteration: unknown/initial state enter")

                self.point = 0
                self._state_transition(self.State.calibrate)

    @classmethod
    def get_series_name(cls, settings):
        return '{} - {}, step: {}'.format(settings['start_pos'], settings['end_pos'], settings['step'])

    @classmethod
    def get_output_fields(cls):
        return OutputFieldSuite(
            chart=ChartField("chart", ["chart"], " ", _("Pozycja [mm]"), _("Napięcie [V]")),
        )

    @classmethod
    def get_input_fields(cls) -> ControlSuite:
        return ControlSuite(
            NumberControl("start_pos",
                          label=_("Pozycja początkowa [mm]"),
                          description=_("Pozycja lustra na której zaczynasz eksperyment"),
                          min_value=0,
                          max_value=120,
                          default_value=0),

            NumberControl("end_pos",
                          _("Pozycja końcowa [mm]"),
                          description=_("Pozycja lustra na której kończysz eksperyment"),
                          min_value=121,
                          max_value=240,
                          default_value=240),

            NumberControl("step", _("Krok zmiany pozycji [mm]"),
                          description=_("Krok zmiany pozycji lustra między kolejnymi pomiarami"),
                          min_value=1,
                          max_value=50,
                          default_value=1),
        )

    @classmethod
    def create_result_creators(cls):
        return [
            XYChartResultCreator("chart", read_from=["position", "voltage"]),
        ]


class MichelsonManager(MultiModeManager):
    MODE_MANAGERS = [NormalMode, ]

    def post_power_up_diagnostics(self, diagnostics_level='short'):
        pass

    @classmethod
    def get_mode_managers(cls, config_parser: configparser.ConfigParser) -> typing.List['ExperimentMode']:
        return cls.MODE_MANAGERS

    def pre_power_up_diagnostics(self, diagnostics_level='short'):
        pass
