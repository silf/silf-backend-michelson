# coding=utf-8
import abc


class MichelsonAPI(metaclass = abc.ABCMeta):
    @abc.abstractmethod
    def calibration(self):
        """
        Sets mirror in start position,
        turns on the power for microwave and stepping motor
        """
    
    @abc.abstractmethod
    def set_pos(self, pos):
        """
        move mirror to position nnn.nnn  (0.000 - 240.000 mm)
        """
    
    #@abc.abstractmethod
    #def next_pos(self, step):
        """
        move mirror by step in milimeters
        """
        
    @abc.abstractmethod
    def get_voltage(self):
        """
        Read voltage from microwave detector

        :return: voltage
        """
    
    @abc.abstractmethod
    def get_pos(self):
        """
        Read position of mirror

        :return: position
        """

    @abc.abstractmethod
    def stop(self):
        """
        """

    @abc.abstractmethod
    def is_open(self):
        """
        """

    @abc.abstractmethod
    def close(self):
        """
        """
