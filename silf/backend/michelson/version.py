# coding=utf-8

import logging
from os import path
from configparser import ConfigParser

cp = ConfigParser()
file = path.join(path.dirname(__file__), "version.ini")
with open(file) as f:
    cp.read_file(f)

__VERSION__ = cp['VERSION']['version'].split(".")

logging.getLogger("silf_michelson").\
    info("Running silf.backend.commons version".format(".".join(__VERSION__)))

