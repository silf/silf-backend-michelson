import serial
from silf.backend.michelson.api import MichelsonAPI
from time import sleep
import logging
OK_RESULT = 'OK'


class MichelsonDriver(MichelsonAPI):
    def __init__(self, deviceFilePath, readTimeout=2):
        self.devSerial = serial.Serial(deviceFilePath, baudrate=115200, timeout=readTimeout,
                                       parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, xonxoff=False)
        if not self.devSerial.isOpen():
            raise ValueError("Can not open device: " + deviceFilePath)
        self.pos = 0
        self.voltage = 0
        self.logger = logging.getLogger('michelson')
        sleep(1)

    def _run_cmd(self, cmd):
        if cmd[-1] != '\n':
            cmd += '\n'

        byte_cmd = bytes(cmd, "UTF-8")
        self.logger.debug("Run_CMD: "+str(byte_cmd))
        byte_written = self.devSerial.write(byte_cmd)
        self.logger.debug("Len: "+str(byte_written))
        if len(byte_cmd) != byte_written:
            raise ValueError("Error while communicating with device, bytes written " + str(byte_written)
                             + " failed command is: " + cmd)

        ret = self.devSerial.readline().decode("utf-8")
        self.logger.debug("RunCMD ret: "+str(ret))
        return ret

    def calibration(self):
        """
        Sets mirror in start position,
        turns on the power for microwave and stepping motor
        """
        self.devSerial.flush()
        ret = self._run_cmd("c")
        if ret[:2] != OK_RESULT[:2]:
            raise ValueError("Error while communicating with device, calibration not confirmed")
    
    def set_pos(self, pos):
        """
        move mirror to position nnn.nnn  (0.000 - 240.000 mm)
        """
        self.pos = pos
        ret = self._run_cmd("m {}".format(pos))
        if ret[:2] != OK_RESULT[:2]:
            raise ValueError("Error while communicating with device, set_pos not confirmed")

    def get_voltage(self):
        """
        Read voltage from microwave detector

        :return: voltage
        """
        try:
            self.voltage = float(self._run_cmd("v"))
        except:
            raise ValueError("Error while communicating with device in get_voltage")
        self.logger.debug("Get volt: "+str(self.voltage))
        return self.voltage   
    
    def get_pos(self):
        """
        Read position of mirror

        :return: position
        """
        try:
            self.pos = float(self._run_cmd("p"))
        except:
            raise ValueError("Error while communicating with device in get_pos")
        self.logger.debug("Get pos: "+str(self.pos))
        return self.pos   

    def stop(self):
        self.calibration()
        ret = self._run_cmd("s")
        self.logger.debug("Stop ret: "+str(ret))
        if ret[:2] != OK_RESULT[:2]:
            raise ValueError("Error while communicating with device, stop cmd not confirmed")
        sleep(0.1)

    def is_open(self):
        return self.devSerial.isOpen()

    def close(self):
        self.devSerial.close()
