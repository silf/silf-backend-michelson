from distutils.core import setup
from configparser import ConfigParser
from os import path

from pip.req import parse_requirements

# parse_requirements() returns generator of pip.req.InstallRequirement objects
install_reqs = parse_requirements(path.join(path.dirname(__file__), 'REQUIREMENTS'))

if __name__ == "__main__":

    cp = ConfigParser()
    file = path.join(path.dirname(__file__), "silf__centrifugal_force/version.ini")
    with open(file) as f:
        cp.read_file(f)

    setup(
        name='silf-backend-centrifugal-force',
        version=cp['VERSION']['VERSION'],
        packages=['silf_centrifugal_force', 'silf_centrifugal_force_tests'],
        url='',
        license='',
        author='Silf Team',
        author_email='',
        description='',
        install_requirements = [str(ir.req) for ir in install_reqs]
    )
