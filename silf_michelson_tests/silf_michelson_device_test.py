import unittest
from silf_michelson.michelson_device import MichelsonDevice
from silf.backend.commons.util.config import prepend_current_dir_to_config_file
from time import sleep
import logging
logging.basicConfig(level=logging.DEBUG)
class TestMichelsonDevice(unittest.TestCase):

    def setUp(self):
        #conf = prepend_current_dir_to_config_file('mock_conf.ini')
        conf = prepend_current_dir_to_config_file('conf.ini')
        self.device = MichelsonDevice("TestMichelsonDevice", conf)

    def tearDown(self):
        self.device.tearDown()

    #def test_post_power_up_diagnostics(self):
    #    self.device.post_power_up_diagnostics()
    def test_experiment(self):
        self.device.post_power_up_diagnostics()
        self.device.power_up()
        self.device.apply_settings({'velocity':125})
        self.device.start()
        self.device.loop_iteration()
        sleep(2)
        print(self.device.results)
        self.device.loop_iteration()
        sleep(1.5)
        print(self.device.results)
        self.device.pop_results()
        print(self.device.results)
        self.device.stop()
        self.device._tearDown()
    

if __name__ == "__main__":
    unittest.main()
